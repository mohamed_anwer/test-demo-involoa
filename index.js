/**
 * @format
 */

import {AppRegistry,I18nManager,Alert} from 'react-native';
import App from './src/screens/App';
import {name as appName} from './app.json';

console.disableYellowBox = true
I18nManager.forceRTL(false)
I18nManager.allowRTL(false)
AppRegistry.registerComponent(appName, () => App);
