import React,{Component} from 'react';
import {
  SafeAreaView,StyleSheet,ScrollView,View,Text,StatusBar,TouchableOpacity,
  ImageBackground,Image,ActivityIndicator,I18nManager
} from 'react-native';
import {responsiveHeight,moderateScale,responsiveFontSize} from '../utils/responsiveDimensions'
import axios from 'axios'
import {Icon} from 'native-base'
import * as Animatable from 'react-native-animatable';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";

const imgs = ["https://img.fifa.com/image/upload/t_l4/kxj8u3oztgnjmjhlwpww.jpg","https://www.dw.com/image/48642633_303.jpg","https://i2-prod.mirror.co.uk/incoming/article21682980.ece/ALTERNATES/s615/0_Liverpool-v-Atletico-Madrid-UEFA-Champions-League-Round-of-16-Second-Leg-Anfield.jpg"]


class App extends Component {

  state={
    connection:true,
    loading:true,
    index:0,
    data:null,
  }

  constructor(props){
    super(props)
    //Check Connection
    NetInfo.fetch().then(state => {
      console.log("Is connected?", state.isConnected);
      this.setState({connection:state.isConnected})
    });
  }

  componentDidMount = () => {
    const {index} = this.state
    lor(this);
    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected changes?", state.isConnected);
      if(state.isConnected){
        this.getData()
        this.setState({connection:state.isConnected})
      }
    });

    setInterval(()=>{
      this.setState({index:this.state.index<imgs.length-1?this.state.index+1:0})
      console.log(index)
    },3000)
    console.log('LANG   ',I18nManager.isRTL)
  }

  componentWillUnmount() {
    rol();
  }

  getData = () => {
    axios.get('http://skillzycp.com/api/UserApi/getOneOccasion/389/0')
    .then(response=>{
      console.log("API DATA   ",JSON.parse(response.data))
      this.setState({loading:false,data:JSON.parse(response.data)})
    })
    .catch(error=>{
      console.log("API ERROR   ",error.response)
      this.setState({loading:false})
    })
  }

  header = () =>{
    const {index,data} = this.state
    return(
      <ImageBackground 
       source={{uri:imgs[index]}}
       style={{justifyContent:'space-between',width:wp(100),height:hp(35)}}
       >
         <Animatable.View animation="zoomInUp" duration={1000} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center', marginTop:moderateScale(5), width:wp(97),alignSelf:'center'}}>
            <View style={{flexDirection:'row'}}>
                <TouchableOpacity>
                <Icon name={data.isLiked?'star':'staro'} type='AntDesign' style={{color:data.isLiked?'gold':'white',fontSize:responsiveFontSize(8)}} />
                </TouchableOpacity>

                <TouchableOpacity style={{marginHorizontal:moderateScale(6)}}>
                <Icon name='sharealt' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(8)}} />
                </TouchableOpacity>
            </View>

            <TouchableOpacity style={{marginHorizontal:moderateScale(3)}}>
              <Icon name='angle-right' type='FontAwesome5' style={{color:'white',fontSize:responsiveFontSize(10)}} />
            </TouchableOpacity>
         </Animatable.View>
         
         <Animatable.View animation="slideInUp" duration={1000} style={{flexDirection:'row',alignItems:'center', alignSelf:'flex-start',marginHorizontal:moderateScale(6), marginBottom:moderateScale(8)}}>
           {imgs.map((val,i)=>(
             <View style={{width:i==index?12:8,height:i==index?12:8,borderRadius:i==index?6:4,backgroundColor:'white',marginHorizontal:moderateScale(1)}} />
           ))}
         </Animatable.View>
       </ImageBackground>
    )
  }

  basicsInfo = () =>{
    const {data} = this.state
    const date = new Date(data.date)
    return(
      <Animatable.View animation="zoomInUp" duration={1000} style={{width:wp(94),alignSelf:'center',marginTop:moderateScale(7)}}>
          <Text style={{alignSelf:'flex-end',color:'#d6d4d2',fontWeight:'bold'}}># {data.interest}</Text>
          <Text style={{alignSelf:'flex-end',color:'gray',marginTop:moderateScale(3),fontSize:responsiveFontSize(7),fontWeight:'bold'}}>{data.title}</Text>
          
          <View style={{alignSelf:'flex-end',flexDirection:'row-reverse',alignItems:'center',marginTop:moderateScale(3)}}>
            <Icon name='calendar' type='AntDesign' style={{color:'#d6d4d2',fontSize:responsiveFontSize(8)}} />
            <Text style={{marginHorizontal:moderateScale(3),color:'#d6d4d2',fontSize:responsiveFontSize(5),fontWeight:'bold'}}>{date.toString().substring(0,24)}</Text>
          </View>

          <View style={{alignSelf:'flex-end',flexDirection:'row-reverse',alignItems:'center',marginTop:moderateScale(3)}}>
            <Icon name='pushpin' type='AntDesign' style={{color:'#d6d4d2',fontSize:responsiveFontSize(8)}} />
            <Text style={{marginHorizontal:moderateScale(3),color:'#d6d4d2',fontSize:responsiveFontSize(5),fontWeight:'bold'}}>{data.address}</Text>
          </View>
      </Animatable.View>
    )
  }

  traineeInfo = () =>{
    const {data} = this.state
    return(
      <Animatable.View animation="slideInLeft" duration={1000} style={{width:wp(94),alignSelf:'center',marginTop:moderateScale(2)}}>
        
          <View style={{ alignSelf:'flex-end',flexDirection:'row-reverse',alignItems:'center',marginTop:moderateScale(3)}}>
            <Image source={{uri:imgs[0]}} style={{width:40,height:40,borderRadius:20}} />
            <Text style={{marginHorizontal:moderateScale(5),color:'#d6d4d2',fontSize:responsiveFontSize(5),fontWeight:'bold'}}>{data.trainerName}</Text>
          </View>

          <Text style={{alignSelf:'flex-end', marginTop:moderateScale(2),color:'#cfccca',fontSize:responsiveFontSize(6.5),fontWeight:'bold'}}>{data.trainerInfo}</Text>


      </Animatable.View>
    )
  }

  trainingDetails = () =>{
    const {data} = this.state
    return(
      <Animatable.View animation="zoomInUp" duration={1000} style={{width:wp(94),alignSelf:'center',marginTop:moderateScale(4)}}>
        
        <Text style={{alignSelf:'flex-end', color:'#d6d4d2',fontSize:responsiveFontSize(6.5),fontWeight:'bold'}}>عن الدورة</Text>
        <Text style={{alignSelf:'flex-end',marginTop:moderateScale(4), color:'#d6d4d2',fontSize:responsiveFontSize(5.5),fontWeight:'bold'}}>{data.occasionDetail}</Text>

      </Animatable.View>
    )
  }

  trainingPriceItem = (key,val) =>{
    return(
      <View style={{flexDirection:'row-reverse',justifyContent:'space-between', alignItems:'center', width:wp(94),alignSelf:'center',marginTop:moderateScale(2)}}>
        
        <Text style={{alignSelf:'flex-end', color:'#d6d4d2',fontSize:responsiveFontSize(6),fontWeight:'bold'}}>{key}</Text>
        <Text style={{alignSelf:'flex-end',color:'#d6d4d2',fontSize:responsiveFontSize(5),fontWeight:'bold'}}>{val} SAR</Text>

      </View>
    )
  }

  trainingPrice = () =>{
    const {data} = this.state
    return(
      <Animatable.View animation="slideInLeft" duration={1000} style={{alignItems:'center', width:wp(94),alignSelf:'center',marginTop:moderateScale(4)}}>
        
        <Text style={{marginBottom:moderateScale(3), alignSelf:'flex-end', color:'#d6d4d2',fontSize:responsiveFontSize(6.5),fontWeight:'bold'}}>سعر الدورة</Text>
        {this.trainingPriceItem('الحجز العادى',data.price)}
        {this.trainingPriceItem('الحجز السريع',data.price+50)}
        {this.trainingPriceItem('الحجز المميز',data.price+100)}

      </Animatable.View>
    )
  }

  bookingButton = () =>{
    return(
      <TouchableOpacity style={{backgroundColor:'#6D0F87', width:wp(100),height:hp(8),justifyContent:'center',alignItems:'center', marginTop:moderateScale(10)}}>
        <Text style={{color:'white',fontSize:responsiveFontSize(6.5),fontWeight:'bold'}}>قم بالحجز الان</Text>
      </TouchableOpacity>
    )
  }

  loader = () =>{
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center', backgroundColor:'white'}}>
          <ActivityIndicator />
      </View>
    )
  }

  noConnection = () =>{
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center', backgroundColor:'white'}}>
         <Text>No internet connection :(</Text>
      </View>
    )
  }

  borderLine = () =>{
    return(
      <View style={{borderWidth:0.3,borderColor:'#d6d4d2',marginTop:moderateScale(8), width:wp(100)}}/>
    )
  }
  

  render(){
    const {loading,connection} = this.state
    return(
      connection?
      loading?
      this.loader()
      :
      <View style={{flex:1,backgroundColor:'white'}}>
         {this.header()}
         <ScrollView style={{backgroundColor:'white'}} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
          {this.basicsInfo()}
          {this.borderLine()}
          {this.traineeInfo()}
          {this.borderLine()}
          {this.trainingDetails()}
          {this.borderLine()}
          {this.trainingPrice()}
          {this.bookingButton()}
         </ScrollView>
      </View>
      :
      this.noConnection()
      )
  }
}

export default App